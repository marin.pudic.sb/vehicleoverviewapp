import { Injectable } from '@angular/core';
import { Observable, map, of } from 'rxjs';
import { VEHICLES } from './vehicle.constants';
import { VehicleModel } from './vehicle.model';

@Injectable({
  providedIn: 'root',
})
export class VehicleService {
  constructor() { }

  public getVehicles(): Observable<VehicleModel[]> {
    return of(VEHICLES);
  }

  searchVehicles(searchTerm: string): Observable<VehicleModel[]> {
    return of(VEHICLES).pipe(
      map((vehicles: any[]) =>
        vehicles.filter(vehicle =>
          vehicle.name.toLowerCase().includes(searchTerm.toLowerCase())
        )
      )
    );
  }
}
