import { Component, OnInit } from '@angular/core';
import { VehicleService } from 'src/app/core/vehicle/vehicle.service';
import { VehicleModel } from 'src/app/core/vehicle/vehicle.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { VehicleNamePipe } from 'src/app/vehicle-name.pipe';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss'],
  providers:[VehicleNamePipe]
})
export class VehicleListComponent implements OnInit {
  vehicles$: Observable<VehicleModel[]> | undefined;
  searchTerm = '';
  displayedVehicles: VehicleModel[] = [];
  allVehicles: VehicleModel[] = [];
  filteredVehicles: VehicleModel[] = []; 

  constructor(private vehicleService: VehicleService, private router: Router, private vehicleNamePipe: VehicleNamePipe) { }

  ngOnInit(): void {
    this.vehicles$ = this.vehicleService.getVehicles();
    this.vehicles$.subscribe(vehicles => {
      this.allVehicles = vehicles;
      this.search()
    });
  }

  search(): void {
    this.updateDisplayedVehicles();
  }
  
  onInputChange(): void {
    if (!this.searchTerm.trim()) {
      this.displayedVehicles = this.allVehicles.slice(0, 15);
    }
  }

  private updateDisplayedVehicles(): void {
    const searchTerm = this.searchTerm.trim().toLowerCase();
    if (!searchTerm) {
      this.displayFirst15Vehicles();
    } else {
      this.filterVehicles(searchTerm);
    }
  }
  
  private displayFirst15Vehicles(): void {
    this.displayedVehicles = this.allVehicles.slice(0, 15);
  }
  
  private filterVehicles(searchTerm: string): void {
    const filtered = this.allVehicles.filter(vehicle =>
      this.vehicleNamePipe.transform(vehicle.name).toLowerCase().includes(searchTerm) ||
      vehicle.manufacturer.toLowerCase().includes(searchTerm) ||
      vehicle.model.toLowerCase().includes(searchTerm)
    );
    this.displayedVehicles = filtered.slice(0, 15);
  }


  signOut(): void {
    this.router.navigate(['/login']);
  }
}
