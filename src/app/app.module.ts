  import { NgModule } from '@angular/core';
  import { BrowserModule } from '@angular/platform-browser';
  import { AppRoutingModule } from './app-routing.module';
  import { AppComponent } from './app.component';
  import { VehicleService } from './core/vehicle/vehicle.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login/login.component';
import { VehicleListComponent } from './vehicle-list/vehicle-list/vehicle-list.component';
import { TableModule } from 'primeng/table';
import {AutoCompleteModule} from 'primeng/autocomplete'
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { VehicleNamePipe } from './vehicle-name.pipe';

  @NgModule({
    declarations: [AppComponent, LoginComponent, VehicleListComponent, VehicleNamePipe],
    imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule, FormsModule, TableModule, AutoCompleteModule, InputTextModule, 
      ButtonModule ],
    providers: [VehicleService],
    bootstrap: [AppComponent],
  })
  export class AppModule {}