import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });

    this.loginForm.get('password')?.valueChanges.subscribe(value => {
      const hasNumber = /\d/.test(value);
      const hasUpper = /[A-Z]/.test(value);
      const hasSpecial = /[!@#$%^&*(),.?":{}|<>]/.test(value);
      const isValid = hasNumber && hasUpper && hasSpecial && value.length >= 8;
      
      if (isValid) {
        this.loginForm.get('password')?.setErrors(null);
      } else {
        this.loginForm.get('password')?.setErrors({ invalidPassword: true });
      }
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      
      //suvišni kod koji zamjenjuje pravi kod koji bi inače išao tu da postoji baza podataka itd...
      const username = this.loginForm.get('username')?.value;
      const password = this.loginForm.get('password')?.value;

      console.log('Username:', username);
      console.log('Password:', password);
      this.router.navigate(['/vehicle-list']);
    }
  }
}
